sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/BusyIndicator"
], function(Controller, BusyIndicator) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Folder", {
		onInit: function() {
			var self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			$.ajax("/Flow7Api/api/diagram")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					self.getOwnerComponent().setModel(oData, "diagrams");
				});
		},
		onFloderPress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("diagrams");
			var oItem = oObject.getModel().getProperty(oObject.getPath());
			this._oRouter.navTo("formlist", {
				folderId: oItem.FolderGuid,
				query: {
					folderName: oItem.FolderName
				}
			});
		}
	});
});